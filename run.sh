#!/usr/bin/env sh

NAME=`date +"%T_on_%m_%d"`

# Generating HTML and put it public
cd /opt/dudesvsdudes/dudesvsdudes/
doxygen Doxyfile
cd /opt/dudesvsdudes/dvd-engine-docs/
rm -rf /var/www/engine-docs/*
cp -R   /opt/dudesvsdudes/dudesvsdudes/doc/html/* /var/www/engine-docs
cp -R /var/www/engine-docs $NAME

cp -R /opt/dudesvsdudes/dudesvsdudes/doc .
mv doc $NAME
cd $NAME
mv doc/* .
rm -rf d*
cd ..
zip -r $NAME.zip $NAME
rm -rf $NAME

mv $NAME.zip archives/
