#!/usr/bin/env sh

git add *
git commit -m "`date +"%T_on_%m_%d"`"
git push origin master
